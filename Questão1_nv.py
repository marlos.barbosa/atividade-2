from email.quoprimime import body_length


def collecting_data():
    body_mass = float(input("Digite seu peso (kg):"))
    body_length = float(input("Digite sua altura (m):"))
    return body_mass, body_length

def calculating_body_mass_index(body_mass,body_lenght):
    body_mass_index = ((body_mass)/(body_lenght**2))
    return body_mass_index

def print_information (body_mass_index):
    print(f"{round(body_mass_index, 2)}") #olhar esse print

    if body_mass_index < 17:
        print("Muito abaixo do peso")
    elif (body_mass_index < 18.5):
        print("Abaixo do peso")
    elif (body_mass_index < 25):
        print("Peso normal")
    elif (body_mass_index < 30):
        print("Sobrepeso")
    elif (body_mass_index < 35):
        print("Obesidade grau I")
    elif (body_mass_index < 40):
        print("Obesidade grau II")
    else:
        print("Obesidade grau III")

weight,height = collecting_data ()
BMI = calculating_body_mass_index (weight,height)
print_information(BMI)
