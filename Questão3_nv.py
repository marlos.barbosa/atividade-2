def counting_oxen():
    quantity = int(input("Quantos bois Ambrósio possui?"))
    return quantity

def collecting_oxen_data(quantity):
    full_ox_information = []
    ox_weight = []
    for i in range (0, quantity):
        indicator = i+1
        one_ox_data = [str(i) for i in input("Dados do %dº boi (identificador, peso e nome separados por espaço):" % indicator).split()]
        one_ox_data[0]=int(one_ox_data[0])
        one_ox_data[1]=float(one_ox_data[1])
        ox_weight.append(one_ox_data[1])
        full_ox_information.append(one_ox_data)
    return full_ox_information, ox_weight

def sorting_oxen_by_weight(ox_weight):
    ox_weight.sort()
    return ox_weight

def creating_empty_lists_for_id_and_name(quantity):
    ox_id = []
    ox_name = []
    for i in range(0,quantity):
        ox_id.append(0)
        ox_name.append(0)
    return ox_id, ox_name

def selecting_interest_values(quantity,ox_weight,full_ox_information,ox_id,ox_name):
    for i in range(0, quantity):
        if ox_weight[0] == full_ox_information[i][1]:
            ox_id[0]=full_ox_information[i][0]
            ox_name[0] = full_ox_information[i][2]
        if ox_weight[quantity-1]==full_ox_information[i][1]:
            ox_id[1]=full_ox_information[i][0]
            ox_name[1]=full_ox_information[1][2]
    return ox_id, ox_name

def print_information(ox_weight,ox_id,ox_name,quantity):
    print(f"Gordo: id:{ox_id[1]} | peso: {round(ox_weight[quantity-1],2)} | nome:{ox_name[1]}")
    print(f"Magro: id:{ox_id[0]} | peso: {round(ox_weight[0],2)} | nome:{ox_name[0]}")


oxen_quantity = counting_oxen()
oxen_full_data, oxen_weight = collecting_oxen_data(oxen_quantity)
oxen_weight = sorting_oxen_by_weight(oxen_weight)
oxen_id, oxen_name = creating_empty_lists_for_id_and_name(oxen_quantity)
sorted_oxen_id, sorted_oxen_name = selecting_interest_values(oxen_quantity, oxen_weight,oxen_full_data,oxen_id, oxen_name)
print_information(oxen_weight,sorted_oxen_id,sorted_oxen_name,oxen_quantity)