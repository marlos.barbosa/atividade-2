const prompt = require('prompt')
const readNumber = require(`./countingValue_1`)
const readArray = require(`./countingValue_2`)
const fn = async ()=>{
    prompt.start();
    const interestValue = await readNumber(prompt);
    const array = await readArray(prompt);
    console.log(`${interestValue} apareceu ${array.filter(x => x===interestValue).length} vezes`)
}
fn();