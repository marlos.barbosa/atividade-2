const prompt = require('prompt')
const readInfo = require('./analyzingOxen_2')
const fn3 = async()=>{
    prompt.start();
    let oxenData = await readInfo(prompt);
    oxenData = oxenData.map(({ id, weight, name}) => ({ id: parseInt(id,10), weight: parseFloat(weight), name: name}));
    oxenData.sort((a, b) => parseFloat(a.weight) - parseFloat(b.weight));
    const total = oxenData.length
    console.log(`Gordo: id: ${oxenData[total-1].id} | weight: ${oxenData[total-1].weight} kg | name: ${oxenData[total-1].name}`)
    console.log(`Magro: id: ${oxenData[0].id} | weight: ${oxenData[0].weight} kg | name: ${oxenData[0].name}`)
}
fn3()