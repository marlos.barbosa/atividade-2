const readQuantity = require(`./analyzingOxen_1`)
const fn2 = async (prompt) =>{
    const quantityOxen = await readQuantity(prompt);
    const arrayOfOxenData = [];
    let i = 0;
    do {
        data = await prompt.get(['id','weight','name']);
        arrayOfOxenData.push(data);
        i = i+1
    }while(i<quantityOxen)
    return arrayOfOxenData
}

module.exports = fn2